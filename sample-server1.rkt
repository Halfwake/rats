#lang racket

(define (resource-path file-name)
  (build-path "static" file-name))

(define (resource-path-string file-name)
  (path->string (resource-path file-name)))

(define question-style-file (resource-path-string "question-style.css"))
(define answer-style-file (resource-path-string "answer-style.css"))
(define section-style-file (resource-path-string "section-style.css"))

(define test-paths (list (resource-path-string "test1.rts")))

(map (lambda (test-path)
       (with-input-from-file test-path
         (lambda ()
           (read))))
     test-paths)


